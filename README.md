# RuneScape Global Bot API #

*When Java was first created, it was created to "write once, run anywhere".*

With RuneScape bots, this is not the case: you have to adapt your scripts for each bot!

But no longer, because the RuneScape Global Bot API - or RGBA for short - will wrap around your script so that you can truly write once and run anywhere!

**This repository will receive a history rewrite for version 1.0****