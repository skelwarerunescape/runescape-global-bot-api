package test.skelware.util;

import com.skelware.bot.util.StringUtil;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public final class StringUtilTest {

    @Test
    public void testToTitle() throws Exception {
        assertEquals("Hello", StringUtil.toTitle("HELLO"));
        assertEquals("Hello World", StringUtil.toTitle("HELLO_WORLD"));
        assertEquals("Hello world", StringUtil.toTitle("Hello World"));
        assertEquals("Hello world and Son", StringUtil.toTitle("hElLo wOrLD aNd_sON"));
    }
}
