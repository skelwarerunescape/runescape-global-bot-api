package test.skelware.bot.skills;

import com.skelware.bot.skills.Skills;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public final class SkillsTest {

    @Test
    public void testGetExperienceAt() throws Exception {
        assertEquals(0, Skills.getExperienceAt(Integer.MIN_VALUE));
        assertEquals(13034431, Skills.getExperienceAt(99));
        assertEquals(188884740, Skills.getExperienceAt(126));
        assertEquals(188884740, Skills.getExperienceAt(Integer.MAX_VALUE));
    }

    @Test
    public void testGetExperienceBetween() throws Exception {
        assertEquals(13034431, Skills.getExperienceBetween(1, 99));
        assertEquals(13034431, Skills.getExperienceBetween(99, 1));
        assertEquals(188884740, Skills.getExperienceBetween(1, 126));
        assertEquals(188884740, Skills.getExperienceBetween(126, 1));
    }

    @Test
    public void testGetLevelAt() throws Exception {
        assertEquals(1, Skills.getLevelAt(Integer.MIN_VALUE));
        assertEquals(7, Skills.getLevelAt(666));
        assertEquals(10, Skills.getLevelAt(1337));
        assertEquals(126, Skills.getLevelAt(Integer.MAX_VALUE));
    }

    @Test
    public void testGetLevelsBetween() throws Exception {
        assertEquals(3, Skills.getLevelsBetween(666, 1337));
        assertEquals(125, Skills.getLevelsBetween(Integer.MIN_VALUE, Integer.MAX_VALUE));
    }
}
