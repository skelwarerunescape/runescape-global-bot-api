package test.skelware.bot.util;

import com.skelware.bot.data.Fish;
import com.skelware.bot.data.Ore;
import com.skelware.bot.util.DataUtil;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public final class DataUtilTest {

    @Test
    public void testForName() throws Exception {
        assertEquals(Fish.SALMON, DataUtil.forName("Salmon", Fish.values()));
        assertNull(DataUtil.forName("Stephan Bijzitter", Ore.values()));
    }
}
