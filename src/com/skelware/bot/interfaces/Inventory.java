package com.skelware.bot.interfaces;

import com.skelware.bot.items.Item;
import com.skelware.bot.wrappers.Filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Inventory {

    private static Provider provider;

    public static void setProvider(Provider provider) {
        Inventory.provider = provider;
    }

    public static List<Item> getItems() {
        return provider.getItems();
    }

    public static List<Item> getItems(Filter<Item> filter) {
        final List<Item> list = new ArrayList<>();
        final List<Item> items = getItems();
        for (final Item item : items) {
            if (filter.accept(item)) {
                list.add(item);
            }
        }
        return list;
    }

    public static List<Item> getItems(int... ids) {
        Arrays.sort(ids);
        return getItems(new Filter<Item>() {

            @Override
            public boolean accept(Item item) {
                return Arrays.binarySearch(ids, item.getId()) >= 0;
            }
        });
    }

    public static List<Item> getItems(String... names) {
        return getItems(new Filter<Item>() {

            @Override
            public boolean accept(Item item) {
                final String name = item.getName();
                for (final String n : names) {
                    if (n.equals(name)) {
                        return true;
                    }
                }
                return false;
            }
        });
    }

    public static boolean isEmpty() {
        return getItems().isEmpty();
    }

    public static boolean isFull() {
        return getItems().size() == 28;
    }

    public static interface Provider {
        List<Item> getItems();
    }
}
