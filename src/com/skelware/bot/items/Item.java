package com.skelware.bot.items;

import com.skelware.bot.wrappers.Nameable;

public interface Item extends Nameable {
    int getId();

    boolean interact(String action);

    boolean interact(String action, String owner);
}
