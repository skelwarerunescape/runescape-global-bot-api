package com.skelware.bot.wrappers;

/**
 *
 */
public interface Nameable {
    public String getName();
}
