package com.skelware.bot.wrappers;

import java.util.HashMap;

/**
 * The Supplier wrapper makes it easier to handle things that have a close relation to each other.
 */
@SuppressWarnings("serial")
public class Supplier extends HashMap<Integer, Integer> {

    /**
     * Constructs a new Supplier.
     *
     * @param identifiers Key-value pairs. Each even index is a key and each uneven index is the value.
     */
    public Supplier(final int... identifiers) {
        if (identifiers.length % 2 != 0) {
            throw new IllegalArgumentException("Length of identifiers is uneven with " + identifiers.length);
        }

        for (int i = 0; i < identifiers.length; i++) {
            put(identifiers[i], identifiers[++i]);
        }
    }
}
