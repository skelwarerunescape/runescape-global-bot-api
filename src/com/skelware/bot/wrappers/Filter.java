package com.skelware.bot.wrappers;

public interface Filter<T> {
    boolean accept(T t);
}
