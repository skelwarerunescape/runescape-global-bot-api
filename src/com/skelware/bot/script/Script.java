package com.skelware.bot.script;

public interface Script {

    public boolean _start();

    public void  _pause();

    public void _resume();

    public boolean _isPaused();

    public boolean _stop();

    public boolean _beforeStart(String... args);

    public boolean _beforeStop();
}
