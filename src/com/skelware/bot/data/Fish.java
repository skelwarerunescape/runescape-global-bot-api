package com.skelware.bot.data;

import com.skelware.bot.wrappers.Nameable;
import com.skelware.bot.util.StringUtil;

/**
 * An enumeration of all Fishes in the game.
 * <p>
 * TODO: Add Oldschool RuneScape implementation for health
 * TODO: Add RuneScape 3 implementation for dynamic health
 */
public enum Fish implements Nameable {
    SHRIMPS(317, 315, 1, 1, 200, false),
    CRAYFISH(13435, 13433, 1, 1, 200, false),
    KARAMBWANJI(3150, 3151, 5, 1, -1, true),
    SARDINE(327, 325, 5, 1, 200, false),
    HERRING(345, 347, 10, 5, 200, false),
    ANCHOVIES(321, 319, 15, 1, 200, false),
    MACKEREL(353, 355, 16, 10, 200, false),
    TROUT(335, 333, 20, 15, 300, false),
    COD(341, 339, 23, 18, 360, false),
    PIKE(349, 351, 25, 20, 400, false),
    SLIMY_EEL(3379, 3381, 28, 28, 100, true),
    SALMON(331, 329, 30, 25, 500, false),
    TUNA(359, 361, 35, 30, 600, false),
    RAINBOW_FISH(10138, 10136, 38, 35, 700, true),
    CAVE_EEL(5001, 5003, 38, 38, 760, true),
    LOBSTER(377, 379, 40, 40, 800, false),
    BASS(363, 365, 46, 43, 860, true),
    LEAPING_TROUT(11328, -1, 48, 1, -1, true),
    SWORDFISH(371, 373, 50, 45, 900, false),
    LAVA_EEL(2148, 2149, 53, 53, -1, true),
    LEAPING_SALMON(11330, -1, 58, 1, -1, true),
    MONKFISH(7944, 7946, 62, 62, 1240, true),
    KARAMBWAN(3142, 3144, 65, 1, 600, true),
    LEAPING_STURGEON(11332, -1, 70, 1, -1, true),
    SHARK(383, 385, 76, 80, 1600, true),
    BARON_SHARK(19947, 19948, 76, 80, 2100, true),
    SEA_TURTLE(395, 397, 79, 82, 1640, true),
    MANTA_RAY(389, 391, 81, 91, 1820, true),
    CAVEFISH(15264, 15266, 85, 88, 1600, true),
    ROCKTAIL(15270, 15272, 90, 93, 1860, true);

    private final String name;
    private final int raw_id;
    private final int cooked_id;
    private final int fishing_level;
    private final int cooking_level;
    private final int health;
    private final boolean member;

    private Fish(final int raw_id, final int cooked_id, final int fishing_level, final int cooking_level, final int health, final boolean member) {
        this.name = StringUtil.toTitle(name());
        this.raw_id = raw_id;
        this.cooked_id = cooked_id;
        this.fishing_level = fishing_level;
        this.cooking_level = cooking_level;
        this.health = health;
        this.member = member;
    }

    /**
     * Gets the name of this Fish.
     *
     * @return The name of this Fish.
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Gets the ID of this Fish when it has not yet been cooked or processed.
     *
     * @return The ID of this Fish.
     */
    public int getRawID() {
        return raw_id;
    }

    /**
     * Gets the ID of this Fish when it has been cooked or processed.
     *
     * @return The ID of the cooked Fish, or -1 if it cannot be cooked or processed.
     */
    public int getCookedID() {
        return cooked_id;
    }

    /**
     * Gets the Fishing level required to fish this Fish.
     *
     * @return The Fishing level, or -1 if this Fish cannot be fished.
     */
    public int getFishingLevel() {
        return fishing_level;
    }

    /**
     * Gets the Cooking level required to cook or process this Fish.
     *
     * @return The Cooking level, or -1 if this Fish cannot be processed.
     */
    public int getCookingLevel() {
        return cooking_level;
    }

    /**
     * Gets the health that this Fish heals at level 99 Constitution.
     *
     * @return The health this Fish can heal.
     */
    public int getHealth() {
        return health;
    }


    /**
     * Gets whether this Fish requires membership to be used.
     *
     * @return <tt>true</tt> if and only if membership is at least partially required.
     */
    public boolean requiresMembership() {
        return this.member;
    }

    @Override
    public String toString() {
        return name;
    }
}
