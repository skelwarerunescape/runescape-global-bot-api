package com.skelware.bot.data;

import com.skelware.bot.wrappers.Nameable;
import com.skelware.bot.util.StringUtil;

/**
 * An enumeration of all Skills on the Hiscores page and in-game.
 */
public enum Skill implements Nameable {
    OVERALL,
    ATTACK,
    DEFENCE,
    STRENGTH,
    HITPOINTS,
    RANGED,
    PRAYER,
    MAGIC,
    COOKING,
    WOODCUTTING,
    FLETCHING(true),
    FISHING,
    FIREMAING,
    CRAFTING,
    SMITHING,
    MINING,
    HERBLORE(true),
    AGILITY(true),
    THIEVING(true),
    SLAYER(true),
    FARMING(true),
    RUNECRAFTING,
    HUNTER(true),
    CONSTRUCTION(true),
    SUMMONING(true),
    DUNGEONEERING(true, 120);

    private final String name;
    private final boolean member;
    private final int max_level;
    private final int index_hiscore = ordinal();
    private final int index_game = index_hiscore - 1;

    private Skill() {
        this(false, 99);
    }

    private Skill(boolean member) {
        this(member, 99);
    }

    private Skill(boolean member, int max_level) {
        this.name = StringUtil.toTitle(name());
        this.member = member;
        this.max_level = max_level;
    }

    /**
     * Gets the name of this Skill.
     *
     * @return The name of this Skill.
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Gets the index of the skill in-game.
     *
     * @return The index of the skill in-game.
     */
    public int getGameIndex() {
        return this.index_game;
    }

    /**
     * Gets the index of the skill on the Hiscores page.
     *
     * @return The index of the skill on the Hiscores page.
     */
    public int getHiscoreIndex() {
        return this.index_hiscore;
    }

    /**
     * Gets the maximum achievable level for this Skill.
     *
     * @return The maximum achievable level.
     */
    public int getMaximumLevel() {
        return this.max_level;
    }

    /**
     * Gets whether this skills requires membership to fully train.
     *
     * @return <tt>true</tt> if and only if membership is at least partially required.
     */
    public boolean requiresMembership() {
        return this.member;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
