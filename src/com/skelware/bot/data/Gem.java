package com.skelware.bot.data;

public enum Gem {
    OPAL(1, 1625, 1609),
    JADE(13, 1627, 1611),
    RED_TOPAZ(16, 1629, 1613),
    SAPPHIRE(20, 1623, 1607),
    EMERALD(27, 1621, 1605),
    RUBY(34, 1619, 1603),
    DIAMOND(43, 1617, 1601),
    DRAGONSTONE(55, 1631, 1615),
    ONYX(67, 1671, 1673);

    private Gem(final int crafting_level, final int uncut_id, final int cut_id) {

    }
}