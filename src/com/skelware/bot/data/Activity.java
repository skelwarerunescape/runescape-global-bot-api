package com.skelware.bot.data;

import com.skelware.bot.wrappers.Nameable;
import com.skelware.bot.util.StringUtil;

/**
 * An enumeration of all Activities on the Hiscores page.
 */
public enum Activity implements Nameable {
    BOUNTY_HUNTER,
    BOUNTY_HUNTER_ROGUES,
    DOMINION_TOWER,
    THE_CRUCIBLE,
    CASTLE_WARS_GAMES,
    BARBARIAN_ASSAULT_ATTACKERS,
    BARBARIAN_ASSAULT_DEFENDERS,
    BARBARIAN_ASSAULT_COLLECTORS,
    BARBARIAN_ASSAULT_HEALERS,
    DUEL_TOURNAMENTS,
    MOBILISING_ARMIES,
    CONQUEST,
    FIST_OF_GUTHIX,
    RESOURCE_RACE,
    ATHLETICS,
    SARADOMIN_TEAM_CONTRIBUTION,
    ZAMORAK_TEAM_CONTRIBUTION;

    private final String name = StringUtil.toTitle(name());
    private final int index_hiscore = ordinal() + 26;

    /**
     * Gets the name of this Activity.
     *
     * @return The name of this Activity.
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Gets the hiscore index of this Activity.
     *
     * @return The index of this Activity.
     */
    public int getHiscoreIndex() {
        return index_hiscore;
    }
}
