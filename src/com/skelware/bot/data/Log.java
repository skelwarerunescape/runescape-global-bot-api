package com.skelware.bot.data;

import com.skelware.bot.util.StringUtil;

public enum Log {
    REGULAR_LOGS(1, 1, 1511, 1276, 1278, 1282, 1286),
    ACHEY_LOGS(1, 1, 2862),
    OAK_LOGS(15, 15, 1521),
    WILLOW_LOGS(30, 30, 1519),
    TEAK_LOGS(35, 35, 6333),
    MAPLE_LOGS(45, 45, 1517),
    HOLLOW_BARK(45, -1, 3239),
    MAHOGANY_LOGS(50, 50, 6332),
    ARCTIC_PINE_LOGS(54, 50, 10810),
    EUCALYPTUS_LOGS(58, 58, 12581),
    YEW_LOGS(60, 60, 1515),
    CHOKING_IVY_LOGS(75, -1, -1),
    MAGIC_LOGS(75, 75, 1513),
    CURSED_MAGIC_LOGS(82, 82, 13567),
    STRAIGHT_ROOTS(83, -1, 21349),
    CURLY_ROOTS(85, 83, 21350),
    BLOODWOOD_logs(85, -1, 24121),;
    //...
    //BLOODWOOD_LOGS();

    private final String name;
    private final int item_id;
    private final int woodcutting_level;
    private final int firemaking_level;
    private final int[] tree_ids;

    private Log(final int woodcutting_level, final int firemaking_level, final int item_id, final int... tree_ids) {
        this.name = StringUtil.toTitle(name());
        this.woodcutting_level = woodcutting_level;
        this.firemaking_level = firemaking_level;
        this.item_id = item_id;
        this.tree_ids = tree_ids;
    }

    public String getName() {
        return name;
    }

    public int getItemID() {
        return item_id;
    }

    public int getWoodcuttingLevel() {
        return woodcutting_level;
    }

    public int getFiremakingLevel() {
        return firemaking_level;
    }

    public int[] getTreeIDs() {
        return tree_ids;
    }

    @Override
    public String toString() {
        return name;
    }
}
