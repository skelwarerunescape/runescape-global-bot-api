package com.skelware.bot.data;

import com.skelware.bot.wrappers.Nameable;
import com.skelware.bot.wrappers.Supplier;
import com.skelware.bot.util.StringUtil;

/**
 * An enumeration of all Bars in the game.
 */
public enum Bar implements Nameable {
    LUNAR_BAR(1, 9077, Ore.LUNAR_ORE, 1),
    BRONZE_BAR(1, 2349, Ore.COPPER_ORE, 1, Ore.TIN_ORE, 1),
    BLURITE_BAR(8, 9467, Ore.BLURITE_ORE, 1),
    IRON_BAR(15, 2351, Ore.IRON_ORE, 1),
    ELEMENTAL_BAR(20, 2893, Ore.ELEMENTAL_ORE, 1, Ore.COAL_ORE, 4),
    SILVER_BAR(20, 2355, Ore.SILVER_ORE, 1),
    STEEL_BAR(30, 2353, Ore.IRON_ORE, 1, Ore.COAL_ORE, 2),
    GOLD_BAR(40, 2357, Ore.GOLD_ORE, 1),
    MITHRIL_BAR(50, 2359, Ore.MITHRIL_ORE, 1, Ore.COAL_ORE, 4),
    ADAMANTITE_BAR(70, 2361, Ore.ADAMANTITE_ORE, 1, Ore.COAL_ORE, 6),
    DRAGONBANE_BAR(77, 21783, Ore.DRAGONBANE_ORE, 1),
    WALLASALKIBANE_BAR(77, 21784, Ore.WALLASALKIBANE_ORE, 1),
    BASILISKBANE_BAR(77, 21785, Ore.BASILISKBANE_ORE, 1),
    ABYSSALBANE_BAR(77, 21786, Ore.ABYSSALBANE_ORE, 1),
    OBSIDIAN_BAR(80, 26123, Ore.OBSIDIAN_SHARD, 12),
    RUNITE_BAR(85, 2363, Ore.RUNITE_ORE, 1, Ore.COAL_ORE, 8);

    private final String name;
    private final int level;
    private final int item_id;
    private final Supplier supplier;

    private Bar(final int level, final int item_id, final Ore ore, final int count) {
        this(level, item_id, new Supplier(ore.getItemID(), count));
    }

    private Bar(final int level, final int item_id, final Ore ore_1, final int count_1, final Ore ore_2, final int count_2) {
        this(level, item_id, new Supplier(ore_1.getItemID(), count_1, ore_2.getItemID(), count_2));
    }

    private Bar(final int level, final int item_id, final Supplier supplier) {
        this.name = StringUtil.toTitle(super.toString());
        this.level = level;
        this.item_id = item_id;
        this.supplier = supplier;
    }

    /**
     * Gets the name of this Bar.
     *
     * @return The name of this Bar.
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Gets the ID of this Bar when it has not yet been cooked or processed.
     *
     * @return The ID of this Bar.
     */
    public int getItemID() {
        return item_id;
    }

    /**
     * Gets the Smithing level required to fish this Bar.
     *
     * @return The Smithing level, or -1 if this Bar cannot be smelted.
     */
    public int getSmithingLevel() {
        return level;
    }

    /**
     * Gets the Supplier for this bar,
     * containing all needed resources and amounts.
     *
     * @return The Supplier for this Bar.
     */
    public Supplier getSupplier() {
        return supplier;
    }

    @Override
    public String toString() {
        return name;
    }
}
