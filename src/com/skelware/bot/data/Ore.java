package com.skelware.bot.data;

import com.skelware.bot.wrappers.Nameable;
import com.skelware.bot.util.StringUtil;

/**
 * An enumeration of all Ores in the game.
 * <p>
 * TODO: Add recent Oldschool RuneScape ores.
 * TODO: Add IDs for empty rocks.
 */
public enum Ore implements Nameable {
    OBSIDIAN_SHARD(-1, 26122),
    CLAY(1, 434),
    RUNE_ESSENCE(1, 1436),
    COPPER_ORE(1, 436),
    TIN_ORE(1, 438),
    LIMESTONE(10, 3211),
    BLURITE_ORE(10, 668),
    IRON_ORE(15, 440),
    ELEMENTAL_ORE(20, 2892),
    DAEYALT_ORE(20, 9632),
    SILVER_ORE(20, 442),
    PURE_ESSENCE(30, 7936),
    COAL_ORE(30, 453),
    SANDSTONE(35, -1),
    SANDSTONE_1KG(-1, 6971),
    SANDSTONE_2KG(-1, 6973),
    SANDSTONE_5KG(-1, 6975),
    SANDSTONE_10KG(-1, 6977),
    GEM_ROCK(40, -1, 9030, 9031, 9032),
    GOLD_ORE(40, 444),
    GRANITE(45, -1),
    GRANITE_500G(-1, 6979),
    GRANITE_2KG(-1, 6981),
    GRANITE_5KG(-1, 6983),
    RUBIUM(46, 12630),
    MITHRIL_ORE(55, 447),
    LUNAR_ORE(60, 9076),
    ADAMANTITE_ORE(70, 449),
    LIVING_MINERALS(73, 15263),
    BANE_ORE(77, 21778),
    DRAGONBANE_ORE(-1, 21779),
    WALLASALKIBANE_ORE(-1, 21780),
    BASILISKBANE_ORE(-1, 21781),
    ABYSSALBANE_ORE(-1, 21782),
    CONCENTRATED_COAL_ORE(77, COAL_ORE.getItemID()),
    CONCENTRATED_GOLD_ORE(80, GOLD_ORE.getItemID()),
    RED_SANDSTONE(81, 23194),
    RUNITE_ORE(85, 451);

    private final String name;
    private final int level;
    private final int item_id;
    private final int[] rock_ids;
    private final boolean mineable;

    private Ore(final int level, final int item_id, final int... rock_ids) {
        this.name = StringUtil.toTitle(name());
        this.level = level;
        this.item_id = item_id;
        this.rock_ids = rock_ids;
        this.mineable = rock_ids != null;
    }

    /**
     * Gets the name of this Ore.
     *
     * @return The name of this Ore.
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Gets the Mining level required to mine this Ore.
     *
     * @return The Mining level, or -1 if it cannot be mined.
     */
    public int getMiningLevel() {
        return this.level;
    }

    /**
     * Gets the ID of this Ore.
     *
     * @return The ID of this Ore.
     */
    public int getItemID() {
        return this.item_id;
    }

    /**
     * Gets the IDs for the rocks containing this Ore.
     *
     * @return The IDs of rocks containing this Ore.
     */
    public int[] getRockIDs() {
        return this.rock_ids;
    }

    /**
     * Gets whether this Ore can be mined or not.
     *
     * @return <tt>true</tt> if and only if this Ore has one or more rock IDs.
     */
    public boolean isMineable() {
        return this.mineable;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
