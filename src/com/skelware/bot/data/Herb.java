package com.skelware.bot.data;

import com.skelware.bot.wrappers.Nameable;
import com.skelware.bot.util.StringUtil;

/**
 * TODO: Documentation
 */
public enum Herb implements Nameable {
    GUAM(199, 249, 9, 1),
    MARRENTILL(201, 251, 14, 5),
    TARROMIN(203, 253, 19, 11),
    HARRALANDER(205, 255, 26, 20),
    RANARR(207, 257, 32, 25),
    TOADFLAX(3049, 2998, 38, 30),
    SPIRIT_WEED(12174, 12172, 36, 35),
    IRIT(209, 259, 44, 40),
    WERGALI(14836, 14854, 46, 41),
    AVANTOE(211, 261, 50, 48),
    KWUARM(213, 263, 56, 54),
    SNAPDRAGON(3051, 3000, 62, 59),
    CADANTINE(215, 265, 67, 65),
    LANTADYME(2485, 2481, 73, 67),
    DWARF_WEED(217, 267, 79, 70),
    TORSTOL(219, 269, 85, 75),
    FELLSTALK(21626, 21624, 91, 91),

    ARDRIGAL(1527, 1528, -1, 3),
    ROGUES_PURSE(1533, 1534, -1, 3),
    SITO_FOIL(1529, 1530, -1, 3),
    SNAKE_WEED(1525, 1526, -1, 3),
    VOLENCIA_MOSS(1531, 1532, -1, 3),

    ERZILLE(19984, 19989, 58, 54),
    UGUNE(19986, 19991, 65, 56),
    ARGWAY(19985, 19990, 70, 57),
    SHENGO(19987, 19992, 76, 58),
    SAMADEN(19988, 19993, 80, 59);

    private final String name;
    private final int grimy_id;
    private final int clean_id;
    private final int farming_level;
    private final int herblore_level;

    private Herb(final int grimy_id, final int clean_id, final int farming_level, final int herblore_level) {
        this.name = StringUtil.toTitle(name());
        this.grimy_id = grimy_id;
        this.clean_id = clean_id;
        this.farming_level = farming_level;
        this.herblore_level = herblore_level;
    }

    /**
     * @return
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * @return
     */
    public int getCleanID() {
        return this.clean_id;
    }


    /**
     * @return
     */
    public int getGrimyID() {
        return this.grimy_id;
    }

    /**
     * @return
     */
    public int getFarmingLevel() {
        return this.farming_level;
    }

    /**
     * @return
     */
    public int getHerbloreLevel() {
        return this.herblore_level;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
