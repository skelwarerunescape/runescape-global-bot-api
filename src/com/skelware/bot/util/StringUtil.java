package com.skelware.bot.util;

/**
 * A function collection that contains many functions related to String operations.
 */
public class StringUtil {

    /**
     * Makes a String beautiful by writing each first character (of a word) uppercase.
     *
     * @param string The String to be written as a title.
     * @return The resulting String written as title.
     */
    public static String toTitle(final String string) {
        final StringBuilder sb = new StringBuilder();
        for (String s : string.split("_")) {
            sb.append(Character.toUpperCase(s.charAt(0)));
            sb.append(s.substring(1, s.length()).toLowerCase());
            sb.append(" ");
        }
        return sb.toString().trim();
    }
}
