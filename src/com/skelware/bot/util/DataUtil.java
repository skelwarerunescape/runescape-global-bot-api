package com.skelware.bot.util;

import com.skelware.bot.wrappers.Nameable;

public class DataUtil {


    /**
     * @param name
     * @param values
     * @param <T>
     * @return
     */
    public static <T extends Nameable> T forName(final String name, final T[] values) {
        for (final T t : values) {
            if (t.getName().equals(name)) {
                return t;
            }
        }
        return null;
    }
}
