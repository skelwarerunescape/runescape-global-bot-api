package com.skelware.bot.input;

public class Input {

    private static Provider provider;
    
    public static void setProvider(Provider provider) {
        Input.provider = provider;
    }

    public static boolean isMouseEnabled() {
        return provider.isMouseEnabled();
    }

    public static boolean isKeyboardEnabled() {
        return provider.isKeyboardEnabled();
    }

    public static boolean setMouseEnabled(boolean enabled) {
        provider.setMouseEnabled(enabled);
        return enabled;
    }

    public static boolean setKeyboardEnabled(boolean enabled) {
        provider.setKeyboardEnabled(enabled);
        return enabled;
    }

    public static interface Provider {

        boolean isMouseEnabled();

        boolean isKeyboardEnabled();

        void setMouseEnabled(boolean enabled);

        void setKeyboardEnabled(boolean enabled);
    }
}
