package com.skelware.bot.skills;

import com.skelware.bot.data.Skill;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * A function collection that contains many functions related to the Skills in RuneScape.
 */
public class Skills {

    private static final int MIN_LEVEL = 1;
    private static final int MAX_LEVEL = 126;
    private static final int[] EXPERIENCE_TABLE = new int[MAX_LEVEL + 1];

    static {
        int value = 0;
        int experience = 0;

        for (int level = MIN_LEVEL; level < EXPERIENCE_TABLE.length; level++) {
            EXPERIENCE_TABLE[level] = value;
            experience += (int) Math.floor(level + 300 * Math.pow(2, level / 7.0));
            value = (int) Math.floor(experience / 4);
        }
    }

    /**
     * Gets the experience required to reach a certain level.
     *
     * @param level The level to be checked for.
     * @return The amount of experience required to reach given level.
     */
    public static int getExperienceAt(int level) {
        return EXPERIENCE_TABLE[Math.max(MIN_LEVEL, Math.min(level, MAX_LEVEL))];
    }

    /**
     * Gets the amount of experience that separates two given levels.
     * The levels do not have to have sorted from low to high.
     *
     * @param beginLevel The first level in the boundary.
     * @param endLevel   The second level in the boundary.
     * @return The difference between the levels expressed in experience.
     */
    public static int getExperienceBetween(int beginLevel, int endLevel) {
        return Math.abs(EXPERIENCE_TABLE[beginLevel] - EXPERIENCE_TABLE[endLevel]);
    }

    /**
     * Gets the level at for a given amount of experience.
     *
     * @param experience The amount of experience.
     * @return The level for the given amount of experience.
     */
    public static int getLevelAt(int experience) {
        if (experience < 0) {
            return MIN_LEVEL;
        } else if (experience >= EXPERIENCE_TABLE[MAX_LEVEL]) {
            return MAX_LEVEL;
        }

        int low = 0;
        int high = EXPERIENCE_TABLE.length - 1;

        while (low < high) {
            int mid = (low + high) / 2;

            int d1 = Math.abs(EXPERIENCE_TABLE[mid] - experience);
            int d2 = Math.abs(EXPERIENCE_TABLE[mid + 1] - experience);

            if (d2 <= d1) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }

        return Math.max(MIN_LEVEL, Math.min(EXPERIENCE_TABLE[low] > experience ? low - 1 : low, MAX_LEVEL));
    }

    /**
     * Gets the amount of levels that separates two given experience values.
     * The experience values do not have to have sorted from low to high.
     *
     * @param beginExperience The first experience value in the boundary.
     * @param endExperience   The second experience value in the boundary.
     * @return The amount of levels between the given experience values.
     */
    public static int getLevelsBetween(int beginExperience, int endExperience) {
        int beginLevel = getLevelAt(beginExperience);
        int endLevel = getLevelAt(endExperience);
        return Math.abs(beginLevel - endLevel);
    }


    /**
     * Tracks SkillData
     */
    public static class Tracker {

        private static Provider provider;
        private static HashMap<Skill, SkillData> map = new HashMap<>();

        /**
         * Sets the Provider for the bot-specific methods for this class.
         *
         * @param provider The Provider that provides access to the current bot's API.
         */
        public static void setProvider(Provider provider) {
            Tracker.provider = provider;
        }

        /**
         * Starts tracking the provided Skills.
         *
         * @param skills The Skills to track.
         */
        public static void start(Skill... skills) {
            setEnabled(true, skills);
        }

        /**
         * Stops tracking the provided Skills.
         *
         * @param skills The Skills to stop tracking.
         */
        public static void stop(Skill... skills) {
            setEnabled(false, skills);
        }

        /**
         * Checks whether the given Skills are tracked or not.
         *
         * @param skills The Skills to check.
         * @return <tt>true</tt> if and only if all provided Skills are bieng tracked.
         */
        public static boolean isEnabled(Skill... skills) {
            boolean match = false;
            for (Skill skill : skills) {
                if (map.get(skill) == null) {
                    return false;
                } else {
                    match = true;
                }
            }
            return match;
        }

        /**
         * Starts or stops tracking the given skills.
         *
         * @param enabled <tt>true</tt> to track or <tt>false</tt> to stop tracking.
         * @param skills  The Skills to either start or stop tracking.
         */
        public static void setEnabled(boolean enabled, Skill... skills) {
            for (Skill skill : skills) {
                SkillData data = map.get(skill);
                if (data == null) {
                    data = new SkillData(skill);
                    map.put(skill, data);
                }
                data.enabled = enabled;
            }
        }

        /**
         * Clears the data for the given Skills.
         *
         * @param skills The Skills to have their data cleared.
         */
        public static void clear(Skill... skills) {
            for (Skill skill : skills) {
                map.remove(skill);
            }
        }

        /**
         * Gets the level in the given Skill when tracking started.
         *
         * @param skill The Skill to check.
         * @return The level in the given Skill when tracking started.
         */
        public static int getStartingLevel(Skill skill) {
            SkillData data = map.get(skill);
            if (data == null) {
                data = new SkillData(skill);
                map.put(skill, data);
            }
            return data.startLevel;
        }

        /**
         * Gets the experience in the given Skill when tracking started.
         *
         * @param skill The Skill to check.
         * @return The experience in the given Skill when tracking started.
         */
        public static int getStartingExperience(Skill skill) {
            SkillData data = map.get(skill);
            if (data == null) {
                data = new SkillData(skill);
                map.put(skill, data);
            }
            return data.startExperience;
        }

        /**
         * Gets the current level in the given Skill.
         *
         * @param skill The Skill to check.
         * @return The current level in the given Skill or <tt>-1</tt> if unknown.
         */
        public static int getCurrentLevel(Skill skill) {
            int current = provider.getCurrentLevel(skill);
            final SkillData data = map.get(skill);

            if (data != null) {
                if (data.level > current) {
                    current = data.level;
                } else {
                    data.level = current;
                }
            }

            return current;
        }

        /**
         * Gets the current experience in the given Skill.
         *
         * @param skill The Skill to check.
         * @return The amount of experience in the given Skill or <tt>-1</tt> if unknown.
         */
        public static int getCurrentExperience(Skill skill) {
            int current = provider.getCurrentExperience(skill);
            final SkillData data = map.get(skill);

            if (data != null) {
                if (data.experience > current) {
                    current = data.experience;
                } else {
                    data.experience = current;
                }
            }

            return current;
        }

        /**
         * Gets the next level in the given Skill.
         *
         * @param skill The Skill to check.
         * @return The next level in the given Skill.
         */
        public static int getNextLevel(Skill skill) {
            return Math.max(MIN_LEVEL, Math.min(getCurrentLevel(skill), MAX_LEVEL));
        }

        /**
         * Gets the amount of levels gained in the given Skill since tracking started.
         *
         * @param skill The Skill to check.
         * @return The amount of levels gained in the given Skill.
         */
        public static int getGainedLevels(Skill skill) {
            int start = getStartingLevel(skill);
            int current = getCurrentLevel(skill);
            return current - start;
        }

        /**
         * Gets the amount of experience gained in the given Skill since tracking started.
         *
         * @param skill The Skill to check.
         * @return The amount of experience gained in the given Skill.
         */
        public static int getGainedExperience(Skill skill) {
            int start = getStartingExperience(skill);
            int current = getCurrentExperience(skill);
            return current - start;
        }


        /**
         * Gets the experience required to advance to the next level.
         *
         * @param skill The Skill to check.
         * @return The amount of experience required to advance to the next level.
         */
        public static int getExperienceTillNextLevel(Skill skill) {
            return getExperienceTillLevel(skill, getCurrentLevel(skill) + 1);
        }

        /**
         * Gets the amount of experience required to advance to a given level.
         *
         * @param skill The Skill to check.
         * @param level The level to set as target for this Skill.
         * @return The amount of experience required to advance to the given level.
         */
        public static int getExperienceTillLevel(Skill skill, int level) {
            int current = getCurrentExperience(skill);
            int target = getExperienceAt(level);
            return Math.max(0, target - current);
        }

        /**
         * Gets the percentage till the next based on the progress since the current level.
         *
         * @param skill The Skill to check.
         * @return The percentage between 0 and 100.
         */
        public static int getPercentageTillNextLevel(Skill skill) {
            int current = getCurrentLevel(skill);
            return getPercentageTillLevel(skill, current + 1, current);
        }

        /**
         * Gets the percentage till the given level based on the progress since the current level.
         *
         * @param skill The Skill to check.
         * @return The percentage between 0 and 100.
         */
        public static int getPercentageTillLevel(Skill skill, int level) {
            return getPercentageTillLevel(skill, getCurrentLevel(skill), level);
        }

        /**
         * Gets the percentage till the given level based on the progress since the given starting level.
         *
         * @param skill The Skill to check.
         * @return The percentage between 0 and 100.
         */
        public static int getPercentageTillLevel(Skill skill, int level, int startLevel) {
            int begin = getExperienceAt(startLevel);
            int target = getExperienceAt(level);
            int current = getCurrentExperience(skill);
            int todo = Math.max(0, target - begin);

            if (todo == 0) {
                return 100;
            }

            int done = Math.max(0, current - begin);

            if (done == 0) {
                return 0;
            }

            return done * 100 / todo;
        }


        /**
         * Gets the experience that is being gained per hour.
         *
         * @param skill The Skill to check.
         * @return The experience per hour for the given Skill.
         */
        public static int getExperiencePerHour(Skill skill) {
            return getExperiencePer(skill, TimeUnit.HOURS);
        }

        /**
         * Gets the experience that is being gained per given TimeUnit.
         *
         * @param skill The Skill to check.
         * @param unit  The TimeUnit to convert the time to.
         * @return The experience per given TimeUnit for the given Skill.
         */
        public static int getExperiencePer(Skill skill, TimeUnit unit) {
            int gained = getGainedExperience(skill);
            long millis = System.currentTimeMillis() - map.get(skill).timer;
            long modifier = TimeUnit.MILLISECONDS.convert(1, unit);
            return (int) ((gained * modifier) / millis);
        }

        /**
         * Gets the time required to advance to the next level.
         *
         * @param skill The Skill to check.
         * @return The time in milliseconds needed to advance to the next level.
         */
        public static long getTimeTillNextLevel(Skill skill) {
            return getTimeTillLevel(skill, getCurrentLevel(skill) + 1, TimeUnit.MILLISECONDS);
        }

        /**
         * Gets the time required to advance to the next level.
         *
         * @param skill The Skill to check.
         * @param unit  The TimeUnit to convert the time to.
         * @return The time in the given TimeUnit needed to advance to the next level.
         */
        public static long getTimeTillNextLevel(Skill skill, TimeUnit unit) {
            return getTimeTillLevel(skill, getCurrentLevel(skill) + 1, unit);
        }

        /**
         * Gets the time required to advance to the next level.
         *
         * @param skill The Skill to check.
         * @param level The level to set as target for this Skill.
         * @return The time in milliseconds needed to advance to the next level.
         */
        public static long getTimeTillLevel(Skill skill, int level) {
            return getTimeTillLevel(skill, level, TimeUnit.MILLISECONDS);
        }

        /**
         * Gets the time requires to advance to a given level.
         *
         * @param skill The Skill to check.
         * @param level The level to set as target for this Skill.
         * @param unit  The TimeUnit to convert the time to.
         * @return The time in the given TimeUnit needed to advance to the given level.
         */
        public static long getTimeTillLevel(Skill skill, int level, TimeUnit unit) {
            long xph = getExperiencePer(skill, unit);
            int target = getExperienceTillLevel(skill, level);
            long modifier = TimeUnit.MILLISECONDS.convert(1, unit);
            return target > 0 && xph > 0 ? (int) ((target * modifier) / xph) : 0;
        }

        /**
         * Contains Skill data
         */
        private static class SkillData {

            private boolean enabled;

            private long timer;
            private int level;
            private int startLevel;
            private int experience;
            private int startExperience;

            private final Skill skill;

            private SkillData(Skill skill) {
                this.skill = skill;
                this.timer = System.currentTimeMillis();
                this.startLevel = getCurrentLevel(skill);
                this.startExperience = getCurrentExperience(skill);

                this.level = this.startLevel;
                this.experience = this.startExperience;
            }
        }

        public static interface Provider {
            public int getCurrentLevel(Skill skill);

            public int getCurrentExperience(Skill skill);
        }
    }
}
