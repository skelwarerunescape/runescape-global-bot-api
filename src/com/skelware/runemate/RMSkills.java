package com.skelware.runemate;

import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.local.Skill;
import com.skelware.bot.skills.Skills;

public class RMSkills implements Skills.Tracker.Provider {

    @Override
    public int getCurrentLevel(com.skelware.bot.data.Skill skill) {
        if (!RuneScape.isLoggedIn()) {
            return -1;
        }

        return convert(skill).getCurrentLevel();
    }

    @Override
    public int getCurrentExperience(com.skelware.bot.data.Skill skill) {
        if (!RuneScape.isLoggedIn()) {
            return -1;
        }

        return convert(skill).getExperience();
    }

    private Skill convert(com.skelware.bot.data.Skill skill) {
        return Skill.values()[skill.getGameIndex()];
    }
}
