package com.skelware.runemate.bot.interfaces;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.queries.results.SpriteItemQueryResults;
import com.skelware.bot.items.Item;
import com.skelware.runemate.items.RMItem;

import java.util.ArrayList;
import java.util.List;

public class RMInventory implements com.skelware.bot.interfaces.Inventory.Provider {

    @Override
    public List<Item> getItems() {
        final List<Item> list = new ArrayList<>();
        final SpriteItemQueryResults items = Inventory.getItems();
        items.forEach(item -> list.add(new RMItem(item)));
        return list;
    }
}
