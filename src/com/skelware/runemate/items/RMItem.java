package com.skelware.runemate.items;

import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.skelware.bot.items.Item;

public class RMItem implements Item {

    private final SpriteItem item;

    public RMItem(SpriteItem item) {
        this.item = item;
    }

    @Override
    public String getName() {
        return item.getDefinition().getName();
    }

    @Override
    public int getId() {
        return item.getId();
    }

    @Override
    public boolean interact(String action) {
        return item.interact(action);
    }

    @Override
    public boolean interact(String action, String owner) {
        return item.interact(action, owner);
    }
}
