package com.skelware.runemate.input;

import com.runemate.game.api.hybrid.input.Keyboard;
import com.runemate.game.api.hybrid.input.Mouse;
import com.skelware.bot.input.Input;

public class RMInput implements Input.Provider {

    @Override
    public boolean isMouseEnabled() {
        return Mouse.isInputAllowed();
    }

    @Override
    public boolean isKeyboardEnabled() {
        return Keyboard.isInputAllowed();
    }

    @Override
    public void setMouseEnabled(boolean enabled) {
        if (Mouse.isInputAllowed() != enabled) {
            Mouse.toggleInput();
        }
    }

    @Override
    public void setKeyboardEnabled(boolean enabled) {
        if (Keyboard.isInputAllowed() != enabled) {
            Keyboard.toggleInput();
        }
    }
}
