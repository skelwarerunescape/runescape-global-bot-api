package com.skelware.runemate.script.herblore;

import com.skelware.bot.data.Herb;
import com.skelware.bot.interfaces.Inventory;
import com.skelware.runemate.script.RMScript;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class HerblawSalvation extends RMScript {

    private final List<Herb> herbs = Arrays.asList(Herb.values());

    public HerblawSalvation() {
        Collections.reverse(herbs);
    }

    @Override
    public boolean _beforeStart(String... args) {
        return false;
    }

    @Override
    public void onLoop() {
        herbs.forEach(herb -> Inventory.getItems(herb.getGrimyID()).forEach(item -> item.interact("Clean")));
    }

    @Override
    public boolean _beforeStop() {
        return false;
    }
}
