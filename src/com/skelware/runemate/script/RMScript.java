package com.skelware.runemate.script;

import com.runemate.game.api.client.paint.PaintListener;
import com.runemate.game.api.script.framework.LoopingScript;
import com.skelware.bot.input.Input;
import com.skelware.bot.interfaces.Inventory;
import com.skelware.bot.skills.Skills;
import com.skelware.runemate.RMSkills;
import com.skelware.runemate.bot.interfaces.RMInventory;
import com.skelware.runemate.input.RMInput;

import java.awt.*;

public abstract class RMScript extends LoopingScript implements com.skelware.bot.script.Script, PaintListener {

    public RMScript() {
        Input.setProvider(new RMInput());
        Skills.Tracker.setProvider(new RMSkills());
        Inventory.setProvider(new RMInventory());
    }

    @Override
    public final void onStart(String... args) {
        Input.setKeyboardEnabled(true);
        Input.setMouseEnabled(true);

        this.getEventDispatcher().addListener(this);
        //this.setLoopDelay(100, 500);

        if (!this._beforeStart(args)) {
            stop();
        }
    }

    @Override
    public void onLoop() {

    }

    @Override
    public final void onStop() {
        this._beforeStop();
    }

    @Override
    public final boolean _start() {
        return false;
    }

    @Override
    public final void _pause() {
        this.pause();
    }

    @Override
    public final void _resume() {
        this.resume();
    }

    @Override
    public final boolean _isPaused() {
        return false;
    }

    @Override
    public final boolean _stop() {
        return false;
    }

    @Override
    public final void onPaint(Graphics2D g) {

    }
}
