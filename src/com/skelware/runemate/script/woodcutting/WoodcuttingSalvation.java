package com.skelware.runemate.script.woodcutting;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.skelware.bot.data.Log;
import com.skelware.bot.data.Skill;
import com.skelware.bot.interfaces.Inventory;
import com.skelware.bot.skills.Skills;
import com.skelware.runemate.script.RMScript;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class WoodcuttingSalvation extends RMScript {

    private long timer = System.currentTimeMillis();
    private final List<Log> logs = Arrays.asList(Log.values());

    public WoodcuttingSalvation() {
        Collections.reverse(logs);
    }

    @Override
    public boolean _beforeStart(String... args) {
        return true;
    }

    @Override
    public void onLoop() {
        final Player player = Players.getLocal();
        if (player == null || player.getAnimationId() >= 0 || player.isMoving()) {
            timer = System.currentTimeMillis() + 500;
            return;
        }

        if (Inventory.isFull()) {
            logs.forEach(log -> Inventory.getItems(log.getItemID()).forEach(item -> item.interact("Drop")));
            return;
        }

        if (System.currentTimeMillis() < timer) {
            return;
        }

        final int level = Skills.Tracker.getCurrentLevel(Skill.WOODCUTTING);
        final List<Log> my_logs = logs.parallelStream().filter(log -> log.getWoodcuttingLevel() <= level).collect(Collectors.toList());

        for (final Log log : my_logs) {
            final int[] treeIDs = log.getTreeIDs();
            final GameObject tree = GameObjects.getLoaded(treeIDs).sortByDistance().limit(2).random();

            if (tree != null) {
                if (tree.interact("Chop down")) {
                    Execution.delay(600, 1200);
                }
                return;
            }
        }
    }

    @Override
    public boolean _beforeStop() {
        return false;
    }
}
